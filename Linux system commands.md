## Kernel module commands
```Shell
# Show what modules are loaded
lsmod
# show info on a module
modinfo <module_name>
```
## Using fdisk

- List the disks
```Shell
lsblk
# Or with fdisk, much more detailed
fdisk -l <OPTIONAL: /dev/<disk_id>>
```

- Start partitioning of the disk

```Shell
fdisk /dev/<Disk_id>
```

Some handy commands of fdisk:

| command | description |
| --- | --- |
|  p | print partion info |
| d | delete partition |
| n | create new partition|
| m | show help |
| w | write changes you created |
| t | set a type for a certain partition |


Most common partition types:

| Partition type | fdisk number |
| --- | --- |
| EFI | 1 |
| Linux Filesystem | 20 |
| Linux Swap | 19 |
| Linux LVM | 44 |



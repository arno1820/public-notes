1) Retrieve the PKGBUILD file from the git repository from the [AUR repoistory](https://aur.archlinux.org) by doing a git clone of that repository.
2) CD into the repository
3) run
```Shell
makepkg -si
```
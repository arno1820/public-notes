1) Find a usb stick that has at least the storage capacity of the ISO of data available. Format it to FAT.
	- best to have a bit more capacity than the iso to avoid copying issues 

3) Download iso, preferably through the torrent and through the official means!

4) Verify the signature ([more info](https://wiki.archlinux.org/title/Installation_guide#Verify_signature))

```Shell
gpg --keyserver-options auto-key-retrieve --verify <name_of_iso>.iso.sig
```

5) Copy the ISO to the drive:
	- On **Linux** ([more info](https://wiki.archlinux.org/title/USB_flash_installation_medium))
	```Shell
	cp path/to/archlinux-version-x86_64.iso /dev/disk/by-id/usb-My_flash_drive
	```
	- On **Windows** I recommend to use [Rufus](https://github.com/pbatard/rufus)

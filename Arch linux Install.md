_Please always use this guide with the official Arch fora, they are very well updated and explain parts in more detail! When you find something that needs clarification or needs to be updated, please do so._

[Installation guide - ArchWiki](https://wiki.archlinux.org/title/Installation_guide)

Installation guide

[ArchWiki](https://wiki.archlinux.org/)

Arch wiki, one of the biggest benefits on running Arch imho!

## [[Creating a bootable drive]]

- Create it with an ISO from here : [Arch Linux - Downloads](https://archlinux.org/download)
- Verify the signature, also present on the download page of Arch linux


## Boot from USB and do the temporary system set-up

1) Boot from the USB through the motherboard settings.

2) Then you are booted into the live environment. Now you need to configure it so it supports your system ([more info](https://wiki.archlinux.org/title/Installation_guide#Boot_the_live_environment)).

3) Set your locale

```Shell
# list the available keymaps

ls /usr/share/kbd/keymaps/**/*.map.gz

# load the correct keymap ex.: 'de_latin1'

loadkeys <keymap_name>
```

4) Check if the system is booted in UEFI mode (recommended). Make sure your system support UEFI else this can be omitted. If the system is not booted in UEFI refer to the motherboards manual to do so.
```Shell
# When this doesn't work the system is not loaded in UEFI
ls /sys/firmware/efi/efivars
```

5) Connect the internet.
	- If your system is connected through ethernet this should already be done.
	- If your system requires WIFI use [iwctl](https://wiki.archlinux.org/title/Iwctl).
	- If your system requires mobile network use [mmcli](https://wiki.archlinux.org/title/Mmcli).

```Shell
# verify the connection by pinging archlinux.org
ping archlinux.org
``` 

6) Update your system clock
```Shell
timedatectl
```

7) Update the mirror list

```Shell
pacman -Syyy
```

8) Install emacs (to have a better default text editor)
```Shell
pacman -S emacs
```


## [[Partitioning disks]]

Configuration:

| mount point name | partition | partition type | size |
| --- | --- | --- | --- |
| efi | dev/efi | EFI system partiton (fdisk type 1) | 512MiB |
| boot | /dev/boot | Linux filesystem (fdisk 20) | 512MiB when one kernel will be installed, but when using more provide more space like 2GiB |
| LVM | /dev/lvm | Linux LVM (fdisk 44) | The size you'd like your system to have, mostly the remaining bytes |




## Optional: [[Create LUKS partition]]


## Creating the [[LVM]] set-up

1) Make one Pv for each physical disk/partition included in the system 
2) Make the Vg's that will be used
3) Create for each Vg the Logical volumes for the different partitions
	- Here we are using one Vg containing these Lv's: root, home and swap
	- Remember you can always increase the size of the Lv, it is not a problem if the Lv's are not using up the full Vg space.

| Name | Mount point | recommended size |
| --- | --- | --- |
| root | / | 20GiB or 50GiB |
| home | /home | 128GiB or 256GiB |
| swap |SWAP | Old rule: 2xRAM, when having enough RAM: 1xRAM or when having loads of ram and [s0 suspension is supported](https://wiki.archlinux.org/title/Power_management/Suspend_and_hibernate) this can be less |

4) create the filesystems.
	-  For ext4
	```Shell
mkfs.ext4 /dev/<vg_name>/<lv_name>
```
	- For Swap
```Shell
mkswap /dev/<vg_name/<lv_name_swap>
```

5) Mount the file systems (except swap) in /mnt 
```Shell
mount /dev/<vg_name>/<lv_name_root> /mnt
mkdir /mnt/home
mount /dev/<vg_name>/<lv_name_home> /mnt/home
mkdir /mnt/boot
mount /dev/<boot_partition> /mnt/boot
# EFI partition for UEFI supporting system, required to support multi bootable systems: https://superuser.com/questions/520068/efi-partition-vs-boot-partition
mkdir /mnt/boot/EFI
mount /dev/<efi_partition> /mnt/boot/EFI
```
7) enable swap
```Shell
swapon /dev/<vg_name>/<lv_name_swap>
```

- To have an the configuration and mounted volumes:
```Shell
lsblk
```


## Installation of the system

1) Select the mirrors
	- Nano into the file */etc/pacman.d/mirrorlist* and select the closest/best mirrors for your install
	- This list will be carried over to the new system
	- The [reflector](https://wiki.archlinux.org/title/Reflector) command will do this for you ;)
```Shell
reflector
```

2) Use the *packstrap* script to install the base package, linux kernel and firmware for common hardware.
	- Here the linux kernel is installed, however you can install other kernel types aswell
	- Now you can also install other packages that can be required for your system: 
```Shell
# -K 'init an empty pacman keyring in the target'
pacstrap -K /mnt base linux linux-firmware
```

**Wait and Let the system be installed on the volumes :D**
 -- Check warnings of the scipt to be sure you don't run into issues later!

## Configuration of the system

1) Generate an fstab file, make sure to check this file for errors. It should represent the formatting and partitioning we did earlier.
```Shell
# -U to define by UUID
genfstab -U /mnt >> /mnt/etc/fstab
```
2) Chroot into the new system. After this you are root in the new system.
```Shell
arch-chroot /mnt
```
3) Install all base packages you would like to have in the base system (no desktop managers yet, we'll install them later).
	- The *lvm2* package is required to support LVM!
	- here are some basic essential packages, see what you want from these
```Shell
pacman -S emacs linux-headers amd-ucode base-devel networkmanager wpa_supplicant netctl dialog lvm2 git
```

4) Set the time zone
```Shell
ln -sf /usr/share/zoneinfo/<Region>/<City> /etc/localtime
hwclock --systohc
```
5) Localization; edit */etc/local.gen* and uncomment all needed locales, generate the locales.
```Shell
local-gen
```
6) Set the LANG Variable in */etc/locale.conf*
```Nano
LANG=en_US.UTF-8
```
8) Set the keyboard layout in */etc/vconsole.conf*
	- When you mess this up you'll get errors at mkinitcpio like: "WARNING: consolefont: no font found in configuration"
```Nano
KEYMAP=<keymap>
```

10) Optionally: improve compiling speed of package building. Go to */etc/makepkg.conf*, uncomment and change the max amount of threads.
```Nano
MAKEFLAGS="-j<ALLOWED THREADS>"
```
12) Network config, put your hostname in */etc/hostname*
	- Add the following in */etc/hosts*
```Nano
127.0.0.1	localhost
::1		    localhost
```
12) IMPORTANT: Add the mkinitcpio hook **lvm2** at */etc/mkinitcpio.conf*
	- When you encrypted the disk you need to add encrypt to this aswell! [[Create LUKS partition]]
	- Remove 'consolefont' when no 'FONT' is set in /etc/vconsole.conf
```Nano
HOOKS=(... block lvm2 filesystems ...)
```
13) set the pwd of the root user, add a user and set it's password
```Shell
# change root password
passwd
# add user 
useradd -m -g users -G wheel <username>username
# change 'username' password
passwd <username>

```
14) Edit the sudo-ers file 
```Shell
# Make sure we use emacs as our editor
export EDITOR=emacs
# run visudo
visudo
```
uncomment:
```Nano
 %wheel ALL=(ALL) ALL
```

15) Install mkinitcpio-firmware to surpress warnings in mkinitcpio script and have all required firmware present.
	- sudo su \<username\> into the user's shell
	- [[Manually install aur package]] [[Paru]] or any other [AUR helper](https://wiki.archlinux.org/title/AUR_helpers)
	- Use the aur helper to install mkinitcpio-firmware
	- exit out the user's shell
16) install mkinitcpio 
```Shell
mkinitcpio -P
```

### Bootloader - GRUB
1) Install grub and some utilities
```Shell
pacman -S grub efibootmgr dosfstools mtools os-prober
```
2) Install grub
	- Important to check what to do when having a LUKS partition! [[Create LUKS partition]]
```Shell
# --efi-directory specifies the EFI mountpoint, this is /boot/EFI in this tutorial
# --recheck Delete any existing device map and create a new one if necessary.
grub-install --target=x86_64-efi --efi-directory=/boot/EFI --bootloader-id=GRUB --recheck
```
3) Generate grub config
```Shell
grub-mkconfig -o /boot/grub/grub.cfg
```

## Reboot

*There is only hope left, good luck!*

## Next steps

Please also take a look at the [general recommendations on the Arch wiki](https://wiki.archlinux.org/title/General_recommendations)

> [Install the NVIDIA drivers](https://wiki.archlinux.org/title/NVIDIA)
> [Installing Gnome](https://wiki.archlinux.org/title/GNOME)
> Installing a firewall [Nftables]
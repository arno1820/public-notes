[Great source to learn more about LVMs](https://wiki.archlinux.org/title/LVM) :
First make sure your disk is capable of being used as a physical volume.
```Shell
lvmdiskscan
```

### Creating the physical volume (Pv)

```Shell
pvcreate /dev/<volume_name_that_should_be_used_as_lvm>
```

Track the created physical volume with:
```Shell
pvdisplay
# Or get a summary of the physical volumes
pvscan
```

### Creating a volume group
Volume groups  (Vg) are an abstraction layer on top of physical volumes. You are able to combine or *extend* the Vg with multiple physical volumes, they will appear as one in the Vg.

```Shell
# Create a new Vg
vgcreate <vg_name> <pv_name>
# Extend the Vg with a new Pv
vgextend <vg_name> <pv_name>
```

track the volume group.
```Shell
vgdisplay
```

### Create the logical volumes (Lv)

These will be the ones that can be accessed and used by the systemuser. They reside in a Vg, these are formatted with a filesystem.
```Shell
lvcreate -L <size> <vg_name> -n <lv_name>
# Create a partition with the rest of space
lvcreate -l 100%FREE <vg_name> -n <lv_name>
```

### Extending Lv's

```Shell
lvresize -L <+,-><size><G,M,K,...> --resizefs <vg>/<lv>
```
- <+,- > to enlarge or shrink respectively
- < size >< G,M,K,...> size with unit 
- --resizefs also resize the file system 
	- **Note:** Only _ext2_, [ext3](https://wiki.archlinux.org/title/Ext3 "Ext3"), [ext4](https://wiki.archlinux.org/title/Ext4 "Ext4"), _ReiserFS_ and [XFS](https://wiki.archlinux.org/title/XFS "XFS") [file systems](https://wiki.archlinux.org/title/File_systems "File systems") are supported. For a different type of file system see [#Resizing the logical volume and file system separately](https://wiki.archlinux.org/title/LVM#Resizing_the_logical_volume_and_file_system_separately).
- < vg > the volume group name
- < lv > the logical volume
### [Removing a logical volume](https://wiki.archlinux.org/title/LVM#Removing_a_logical_volume)

First, find out the name of the logical volume you want to remove. You can get a list of all logical volumes with:
```Shell
lvs
```

Next, look up the mountpoint of the chosen logical volume:

```Shell
lsblk
```

Then unmount the filesystem on the logical volume:

```Sell
umount /_mountpoint_
```

Finally, remove the logical volume:

```Shell
 lvremove <volume_group>/<logical_volume>
```

Confirm by typing in `y`.

Make sure to update all configuration files (e.g. `/etc/fstab` or `/etc/crypttab`) that reference the removed logical volume.
You can verify the removal of the logical volume by typing `lvs` as root again